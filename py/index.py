from sys import argv
import numpy as np
from  scipy import ndimage
from moviepy.editor import *
from moviepy.video.tools.segmenting import findObjects
from  moviepy.video.fx.all import *
from  copy import copy
import moviepy.audio.io.AudioFileClip
from  moviepy.video.fx.all import *
from vapory import *
from signal import signal, SIGPIPE, SIG_DFL
from subprocess import call

class TextAnimation:
    screensize = (720,460)
    rotMatrix = lambda s,a: np.array( [[np.cos(a),np.sin(a)], 
                                 [-np.sin(a),np.cos(a)]] )

    
    def __init__(self, txt, color = 'white', font = 'Amiri-Bold'):
        self.txt = txt
        self.color = color
        self.font = font
        
    #Create a video of a given text
    def __cvc(self,txt,position='center'):
        if(position=='center'):
          return CompositeVideoClip( [txt.set_pos(position)],
                        size=self.screensize)
        x,y=position
        return CompositeVideoClip( [txt.set_pos((x,'center'))],
                        size=self.screensize)
        
    #character position at given time. 
    def __non_moving(self,screenpos) :
        return lambda t: screenpos 
    #setting poistion function
    def __moveLetters(self,letters, funcpos):
        return [ letter.set_pos(funcpos((0,0)))
              for i,letter in enumerate(letters)]

    def get_typing(self,duration=0,bg='',sound = ''):
        txtclip= TextClip('|'+self.txt,
            color=self.color, font=self.font,# method="caption",
            fontsize=70)
        #txtclip.set_duration().write_gif("text.gif",fps=15)
        letters= findObjects(self.__cvc(txtclip))
        #print ([letter.screenpos for letter in letters])
        #x= self.__moveLetters(letters, self.__non_moving)
    
        #posi = lambda k: lambda t: (30*k-len('|'+self.txt.replace(" ",""))*15,'center')
        #posi_eval =  [posi (i) for i in range (len(y))]
        
        x = [self.__cvc(TextClip(letter,color=self.color, font=self.font,#method="caption",
            fontsize=70),position = letters[i].screenpos)  for i,letter in enumerate('|'+self.txt.replace(" ",""))]
        
        #cursor position 
        cursor = lambda k: lambda t: (50*k,'center')
        cursor_eval =  [cursor (i) for i in range (len(x))]
        if duration == 0: st = .1
        else: st = duration/len(self.txt)
        clip_n = []
        #typing video
        for i in range(0,len(x)):
            clip_n.append (CompositeVideoClip([x[j] for j in range (1,i+1)]+[x[0].set_pos(cursor_eval[i])],size = self.screensize).set_duration(st))
        #blinking cursor before writing
        blink_fr1 = [TextClip(' '),clip_n[0]]
        blinking1 = [ blink_fr1[i%2].set_duration(2*st) for i in range(6)]

        #blinking cursor after writin
        nob= CompositeVideoClip(x[1:len(x)],size=self.screensize).set_duration(st)
        blink_fr2 = [nob,clip_n[len(clip_n)-1]]
        blinking2 = [ blink_fr2[i%2].set_duration(2*st) for i in range(6)]
        clip=concatenate_videoclips(blinking1+[concatenate_videoclips(clip_n)]+blinking2)       
        if bg != '':
           bg = ImageClip(bg,duration=len(self.txt)*st+4*7*st, transparent=True)
           clip = CompositeVideoClip([bg,clip],size=self.screensize)
        if sound != '':
            ks = AudioFileClip(sound).set_duration(len(self.txt)*st+4*7*st)
            clip = CompositeVideoClip([clip]).set_audio(ks)
        
                                            
        self.clip = clip
        return clip

    def __scene(self,t,direction,t_rev):
        W, H = self.screensize
        cam = Camera('location', [ 0,  0, -7],
                     'look_at',  [ 0,  0,  0])
        light = LightSource([0, 0, -1]) # Light at camera location
        s = Scene(camera = cam, objects = [light],included = ["colors.inc"])
        if direction == 1:
           trans = [ -2,-5*t/2+5, 0] #[ -.5,-.5*t/2, 0]
        else:
           trans = [ -2,5*t-10, 0] #[ -.5,.05*t-.4, 0]                                          
    # Add POV-Ray box with image textured on it
        s =s.add_objects([Text('ttf',  '"'+self.font+'.ttf"','"'+self.txt+'"', 0,0,Pigment(self.color),
                           'rotate', [ -(360/t_rev)*t,0, 0],'translate', trans)])

        return s

    ''' def get_scroll(self,direction=1,bg='',sound = ''):
        #txtclip= TextClip(self.txt,
        #    color=self.color, font=self.font,# method="caption",
        #    fontsize=70)
        #txtvideo= self.__cvc(txtclip).set_duration(1)
        #txtvideo.write_gif("text.gif",fps=15)
        t_rev = 2.0
        W, H = self.screensize
        t_quarter = t_rev/4.0
        clip = VideoClip(lambda t: self.__scene(t,direction,t_rev).render(width=W, height=H, antialiasing=0.1)).subclip(3*t_quarter,t_rev)
        clip = mask_color(clip)
        #bg = ImageClip('bg.jbg',duration=duration, transparent=True)
        #bg = CompositeVideoClip([bg],size=self.screensize)
        #clip = mask_and(clip,bg)
        if bg != '':
           bg = ImageClip(bg,duration=t_quarter, transparent=True)
           clip = CompositeVideoClip([bg,clip],size=self.screensize)
        if sound != '':
            ks = AudioFileClip(sound).set_duration(t_quarter)
            clip = CompositeVideoClip([clip]).set_audio(ks)
        #if duration > 0:
        #    clip = CompositeVideoClip([clip]).speedx(final_duration =duration) 
        #clip = fadein(clip,duration=t_quarter)
        self.clip = clip
        return clip '''

    
    def __vortex(self,screenpos,i,nletters):
        d = lambda t : 1.0/(0.3+t**8) #damping
        a = i*np.pi/ nletters # angle of the movement
        v = self.rotMatrix(a).dot([-1,0])
        if i%2 : v[1] = -v[1]
        return lambda t: screenpos+400*d(t)*self.rotMatrix(0.5*d(t)*a).dot(v)
    
    def __cascade(self,screenpos,i,nletters):
        v = np.array([0,-1])
        d = lambda t : 1 if t<0 else abs(np.sinc(t)/(1+t**4))
        return lambda t: v*400*d(t-0.15*i)

    def __arrive(self,screenpos,i,nletters):
        v = np.array([-1,0])
        d = lambda t : max(0, 3-3*t)
        return lambda t: -400*v*d(t-0.2*i)
    
    def __vortexout(self,screenpos,i,nletters):
        d = lambda t : max(0,t) #damping
        a = i*np.pi/ nletters # angle of the movement
        v = self.rotMatrix(a).dot([-1,0])
        if i%2 : v[1] = -v[1]
        return lambda t: 400*d(t-0.1*i)*self.rotMatrix(-0.2*d(t)*a).dot(v)
    def __bounce(self,screenpos) :
         W,H = 200,75
         D = 3
         r = 10 # radius of the ball
         DJ, HJ = 50, 35 # distance and height of the jumps
         ground = 0.75*H # y-coordinate of the ground
         x = lambda t:(-W/3)+(5*W/3)*(t/D)
         y = lambda t: ground - HJ*4*(x(t) % DJ)*(DJ-(x(t) % DJ))/DJ**2
         return lambda t: (x (t),y (t))

    def __moveLetters2(self,letters, funcpos):
        return [ letter.set_pos(funcpos((0,0),i,len(letters)))
              for i,letter in enumerate(letters)]
    def get_moving(self,direction='arrive',duration=2,bg='',sound = ''):
            letters2 = findObjects(self.__cvc(TextClip(self.txt,
            color=self.color, font=self.font,
            kerning = 1, fontsize=70))) # a list of ImageClips
            letters = [self.__cvc(TextClip(letter,color=self.color, font=self.font,# method="caption", align = 'center',
            fontsize=70),position = letters2[i].screenpos)  for i,letter in enumerate(self.txt.replace(" ",""))]
      
            if direction == 'arrive':
                clip = CompositeVideoClip(self.__moveLetters2(letters, self.__arrive), size = self.screensize).subclip(1,1+duration)
            if direction == 'vortex':
                clip = CompositeVideoClip(self.__moveLetters2(letters, self.__vortex), size = self.screensize).subclip(1,1+duration)
            if direction == 'cascade':
                clip =CompositeVideoClip(self.__moveLetters2(letters, self.__cascade), size = self.screensize).subclip(1,1+duration)
            if direction == 'vortexout':
                clip = CompositeVideoClip(self.__moveLetters2(letters, self.__vortexout), size = self.screensize).set_duration(duration)
            if direction == 'bounce':
                clip = CompositeVideoClip(self.__moveLetters(letters, self.__bounce), size = self.screensize).set_duration(duration)
           
            if bg != '':
                bg = ImageClip(bg,duration=duration, transparent=True)
                clip = CompositeVideoClip([bg,clip],size=self.screensize)
            if sound != '':
                ks = AudioFileClip(sound).set_duration(duration)
                clip = CompositeVideoClip([clip]).set_audio(ks)
            self.clip = clip
            return clip



    def produce(self,clips=[],bg='img/bg.jpg',sound = '',name='my_video'):
        clip = self.clip
        if clips != []:
            clips.append(clip)
            clip  = concatenate_videoclips (clips)
        
        if bg != '':
           bg = ImageClip(bg, transparent=True,duration=clip.duration)
           clip = CompositeVideoClip([bg,clip],size=self.screensize)
        else: clip = CompositeVideoClip([ColorClip(self.screensize,(0,0,0),duration=clip.duration),clip],size=self.screensize)
     
        if sound != '':
            ks = AudioFileClip(sound).set_duration(clip.duration)
            clip = CompositeVideoClip([clip]).set_audio(ks)
        self.clip = clip
        clip.write_videofile(name+'.mp4',fps=15,codec='mpeg4')
        clip.write_gif(name+'.gif',fps=15)
        call(["gifsicle", "-O3", "--lossy=10","--batch", name+".gif"])
        
simpl = TextAnimation(argv[3], argv[4],font=argv[5])
simpl.get_moving(argv[6], int(argv[7]))
''' simpl = TextAnimation('Engage your Audience 12345','Olive')
clip2 = simpl.get_moving('vortex')
simpl = TextAnimation('With these great tools 123',font='impact',color='Turquoise')
clip3 = simpl.get_moving('cascade', 4)
simpl = TextAnimation('That are easy to use', font='Andalus')
clip4 = simpl.get_moving('bounce')
simpl = TextAnimation('And EFFECTIVE!',font='impact',color='red')
simpl.get_moving('vortexout') #no need to store last animation as a clip.
'''

simpl.produce([], argv[1], argv[2])

#$command = "python py/index.py 1'$bg' 2'$audio' 3'$text' 4'$color' 5'$font' 6'$animation' 7'$duration' 2>&1";
