var page = require('webpage').create(),
    address = 'http://localhost/verblive/',
    framerate = 30, // number of frames per second. 24 is a good value.
    counter = 0,
    width = 600,
    height = 400;

page.onConsoleMessage = function(msg, lineNum, sourceId) {
  console.log(msg);
};

page.viewportSize = { width: width, height: height };

page.open(address, function(status) {
    if (status !== 'success') {
        console.log('Unable to load the address!');
        phantom.exit(1);
    } else {  
      console.log('Path loaded and ready to render');
      page.clipRect = { top: 0, left: 0, width: width, height: height };
      
      multipleIncludeJs(page, ["js/gsap/TimelineMax.min.js","js/gsap/TweenLite.min.js", "js/gsap/utils/SplitText.min.js"], function(){
          console.log("loaded JS files");

          if (page.injectJs('js/script.js')) {
            var frames = page.evaluate(function() {
              return getTotalDurationInSeconds();
            }) * framerate;
            console.log(frames + " frames");

            for(var frame = 0; frame < frames; frame++) {
                page.evaluate(function (time) {
                    console.log(time);
                    pauseAnimationAt(2);
                }, frame * (1 / framerate));
                //page.render('/dev/stdout', { format: 'png' });
                
                    setTimeout(function(){ setTimeout(function(){ setTimeout(function(){ setTimeout(function(){
                        setTimeout(function(){
                          page.render('frames/frame_'+frame+'.png', { format: "png" });
                          phantom.exit();
                        },1000);
                    },1);
                      },1);
        },1);
  },1);

                        
            }
                
                //frame++;
            
            phantom.exit();
          }

      });
    }
});

function multipleIncludeJs(page, jsArray, done) {
    if (jsArray.length === 0) {
        done();
        return;
    }
    var url = jsArray.shift();
    page.includeJs(url, function(){ console.log(url);
        multipleIncludeJs(page, jsArray, done);
    });
}